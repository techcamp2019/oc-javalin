package ch.ipt.techcamp

import net.andreinc.mockneat.MockNeat
import net.andreinc.mockneat.unit.user.Emails.emails
import net.andreinc.mockneat.unit.user.Names.names
import java.util.*

object EventGenerator {

    fun generateRandomEvent(): Event {
        return Event(
                id = UUID.randomUUID().toString(),
                name = names().get(),
                email = emails().get()
        )
    }
}