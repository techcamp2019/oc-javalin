package ch.ipt.techcamp

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.databind.util.StdDateFormat
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import io.javalin.Javalin
import io.javalin.JavalinEvent
import kotlin.concurrent.fixedRateTimer

val broker = "tech-cluster-kafka-bootstrap-techcamp2019.23.97.218.111.nip.io:443"
val commandTopic = "command-openaccount"

var isGenerating = false

fun main() {

    var environment: String = System.getenv("ENV") ?: "dev-local"
    val dealer = SimpleKafkaDealer(Config.getConfig(environment))

    val app = Javalin.create()

    Runtime.getRuntime().addShutdownHook(Thread { app.stop() })

    app.get("/events") { ctx ->
        ctx.json(dealer.receive())
    }

    app.post("/events") { ctx ->
        val event = dealer.send()
        ctx.json(event)
    }

    fixedRateTimer(
            name = "command-pump",
            daemon = false,
            initialDelay = 0,
            period = 10*1000
    ) {
        if (isGenerating) {
            val event = dealer.send()
            println("Sent event: $event")
        }
    }

    app.post("/start") { ctx ->
        isGenerating = true
        println("Start generating events")
    }

    app.post("/stop") { ctx ->
        isGenerating = false
        println("Stop generating events")
    }

    app.get("/") { ctx ->
        ctx.result("Hello World")
    }

    app.event(JavalinEvent.SERVER_STARTING) {
        dealer.start()
    }

    app.event(JavalinEvent.SERVER_STOPPING) {
        dealer.stop()
    }

    // Start the server
    app.start(8080)
}

data class Event(
        val id: String,
        val name: String,
        val email: String
)

val jsonMapper = ObjectMapper().apply {
    registerKotlinModule()
    disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
    dateFormat = StdDateFormat()
}