package ch.ipt.techcamp

import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import java.util.*

object Config {

    fun getConfig(env: String): Properties {
        return when(env) {
            "dev" -> getDevProperties()
            "prod" -> getProdProperties()
            else -> throw RuntimeException("Environment $env is not supported")
        }
    }

    private fun getDevProperties(): Properties {
        val props = Properties()
        props["bootstrap.servers"] = "tech-cluster-kafka-bootstrap-techcamp2019.23.97.218.111.nip.io:443"
        props["security.protocol"] = "SSL"
        props["ssl.truststore.location"] = "truststore.jks"
        props["ssl.truststore.password"] = "techcamp"
        props["group.id"] = "event-processor"
        props["metadata.max.age.ms"] = 5000
        props["key.deserializer"] = StringDeserializer::class.java
        props["value.deserializer"] = StringDeserializer::class.java
        props["key.serializer"] = StringSerializer::class.java
        props["value.serializer"] = StringSerializer::class.java

        return props
    }

    private fun getProdProperties(): Properties {
        val props = Properties()
        props["bootstrap.servers"] = "tech-cluster-kafka-bootstrap.techcamp2019.svc.cluster.local:9092"
        props["group.id"] = "event-processor"
        props["metadata.max.age.ms"] = 5000
        props["key.deserializer"] = StringDeserializer::class.java
        props["value.deserializer"] = StringDeserializer::class.java
        props["key.serializer"] = StringSerializer::class.java
        props["value.serializer"] = StringSerializer::class.java

        return props
    }

}