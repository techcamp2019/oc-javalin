package ch.ipt.techcamp

import org.apache.kafka.clients.consumer.Consumer
import org.apache.kafka.clients.consumer.KafkaConsumer
import org.apache.kafka.clients.producer.KafkaProducer
import org.apache.kafka.clients.producer.Producer
import org.apache.kafka.clients.producer.ProducerRecord
import org.apache.kafka.common.serialization.StringDeserializer
import org.apache.kafka.common.serialization.StringSerializer
import org.slf4j.LoggerFactory
import java.time.Duration
import java.util.*

class SimpleKafkaDealer(val props: Properties) {

    private val logger = LoggerFactory.getLogger(javaClass)
    private val consumer = createConsumer()
    private val producer = createProducer()

    private fun createConsumer(): Consumer<String, String> {
        return KafkaConsumer<String, String>(props)
    }

    private fun createProducer(): Producer<String, String> {
        return KafkaProducer<String, String>(props)
    }

    fun start() {
        consumer.subscribe(listOf(commandTopic))
    }

    fun stop() {
        consumer.close()
    }

    @Synchronized
    fun receive(): List<Event> {

        val events = mutableListOf<Event>()

        logger.info("Consuming and processing data")

        val records = consumer.poll(Duration.ofSeconds(5))
        logger.debug("Received ${records.count()} records")

        for (record in records) {
            val eventJson = record.value()

            logger.debug("JSON data: $eventJson")
            try {
                val event = jsonMapper.readValue(eventJson, Event::class.java)
                events.add(event)
                logger.info("ch.ipt.Event: $event")
            } catch (ex: Exception) {
                logger.error(ex.message)
            }

        }

        return events
    }

    fun send(): Event {

        val event = EventGenerator.generateRandomEvent()
        val eventJson = jsonMapper.writeValueAsString(event)

        val futureResult = producer.send(ProducerRecord(commandTopic, eventJson))

        return event
    }
}